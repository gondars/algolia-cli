import minimist from 'minimist';

import { init } from './commands/init';
import { search } from './commands/search';
import { HELP} from "./constants/const_help";
import packagejson from "../package.json";

export function cli(argsArray) {
  const args = minimist(argsArray.slice(2));
  let cmd = args._[0];
  if (args['help'] && !cmd) {
    console.log(HELP);
  } else if (args['version'] && !cmd) {
    console.log(`Algolia-CLI current version: ${packagejson.version}`)
  } else {
    switch (cmd) {
      case 'init':
        init(args);
        break;

      case 'search':
        search(args);
        break;

      case 'version':
        console.log(`Algolia-CLI current version: ${packagejson.version}`);
        break;

      default:
        console.log(HELP);
        break;
    }
  }
}
