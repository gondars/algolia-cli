import clear from 'clear';

import { setAppId, setApiKey, setIndexName, reset } from '../utils/config';
import { APPID_APIKEY_INDEXNAME_PROMPT, SUCCESS, INIT_WELCOME } from '../constants/const_init';
import { createScenario } from "../utils/inquirerFactory";
import { HELP_INIT } from "../constants/const_help";

/**
 * Initializes the CLI with required options to make it work
 * @param args
 *        empty takes you to the interactive config menu.
 *        Takes API Key, Application Id and Index name
 *        (example: --apikey <apikey> --appid <appid> --indexname <indexname>)
 *        --help opens up help menu
 */
export async function init (args) {
  reset();
  if (args['appid'] && args['apikey'] && args['indexname']) {
    setConfiguration(args['appid'], args['apikey'], args['indexname']);
  } else if(args['help']) {
    console.log(HELP_INIT);
  } else {
    clear();
    console.log(INIT_WELCOME);
    const answers = await createScenario(APPID_APIKEY_INDEXNAME_PROMPT);
    setConfiguration(answers.appId, answers.apiKey, answers.indexName);
  }
}

/**
 * Sets the configuration params needed to operate
 * @param appId {string}
 * @param apiKey {string}
 * @param indexName {string}
 */
function setConfiguration(appId, apiKey, indexName) {
	console.log(SUCCESS(appId, apiKey, indexName));
  setAppId(appId);
  setApiKey(apiKey);
  setIndexName(indexName);
}
