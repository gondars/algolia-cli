import { getApiKey, getAppId, getIndexName } from '../utils/config';
import { startSpinner, stopSpinner } from '../utils/spinner';
import { generateTable } from '../utils/tableFactory';
import { HELP_SEARCH } from "../constants/const_help";
import { createScenario } from '../utils/inquirerFactory';

import {
  mergeArrayIntoStringWithNewlines,
  mergeArrayIntoStringWithSpaces,
  splitFacets
} from '../utils/stringOps';

import {
  AFTER_SEARCH_PROMPT,
  EDIT_SEARCH_KEY,
  EDIT_SEARCH_PROMPT,
  SEARCH_PROMPT,
  TABLE_HEADERS,
	SEARCH_WELCOME
} from '../constants/const_search';

import algoliasearch from 'algoliasearch';

/**
 * Entry-point for search command, called by algolia-cli search
 * @param args
 *        empty takes you to the interactive search.
 *        Takes search words (example: samsung galaxy s7)
 *        -f adds facet filters to the search, split by ","
 *        (example: -f "brand:apple",categories:"iphone accessories")
 *        --help opens up help menu
 */
export async function search(args) {
  const client = algoliasearch(getAppId(), getApiKey());
  const index = client.initIndex(getIndexName());
  const searchCriteria = mergeArrayIntoStringWithSpaces(args['_']);
  const facets = typeof args['f'] === 'string' ? splitFacets(args['f']) : '';
  if (searchCriteria || facets) {
    await searchFor(searchCriteria, facets, index).then((table) => {
      console.log(table.toString());
    }).catch((e) => {
      console.log(e);
    });
  } else if (args['help']) {
    console.log(HELP_SEARCH);
  } else {
  	console.log(SEARCH_WELCOME);
    await interactiveSearch(index, true).catch((e) => {
      console.log(e);
    });
  }
}

/**
 * Searching function using "algoliasearch"
 * @param searchCriteria {string} takes full string with spaces (example: samsung galaxy s7)
 * @param facetFilters {Array} takes a string of facet filters and splits them
 * @param index {SearchIndex} takes the "algoliasearch" index for searching
 * @returns {Promise<resolve, reject>} returns table object
 */
export async function searchFor(searchCriteria, facetFilters, index) {
  return new Promise(async (resolve, reject) => {
    startSpinner(`Searching for "${searchCriteria}"`);

    const searchResults = await index.search(searchCriteria, {facetFilters}).catch((e) => {
      stopSpinner();
      reject(e.message);
    });

    if (!searchResults) return;
    const formattedData = formatTableDataForNameCategoriesPrice(searchResults.hits);
    const table = await generateTable(TABLE_HEADERS, formattedData);
    // console.log(table.toString());

    stopSpinner();
    resolve(table);
  })
}

/**
 * Recursive interactive search menu
 * @param index takes the "algoliasearch" index for searching
 * @param start {boolean} checks if it is the initial search
 * @param _searchWords {string} takes full string with spaces (example: samsung galaxy s7)
 * @param _facets {string} takes a string of facet filters and splits them
 * @returns {Promise<void>}
 */
async function interactiveSearch(index, start, _searchWords = '', _facets = '') {
  let searchWords = _searchWords;
  let facets = _facets;

  if (start) {
    const answers = await createScenario(SEARCH_PROMPT);
    searchWords = answers.searchCriteria;
    facets = answers.facets;
  }

  await searchFor(searchWords, splitFacets(facets), index).then(async (table) => {
    console.log(table.toString());
    const afterSearchPrompt = await createScenario(AFTER_SEARCH_PROMPT);

    switch (afterSearchPrompt.afterSearchChoices) {
      case 'New Search':
        await interactiveSearch(index, true);
        break;
      case 'Edit Previous Search':
        await editInteractiveSearch(searchWords, facets, index);
        break;
      case 'Exit':
        break;
    }
  }).catch((e) => {
    console.error(e);
  });

}

/**
 * Recursive edit for the interactive search menu
 * @param searchWords {string} takes full string with spaces (example: samsung galaxy s7)
 * @param facets {string} takes a string of facet filters and splits them
 * @param index takes the "algoliasearch" index for searching
 * @returns {Promise<void>}
 */
async function editInteractiveSearch(searchWords, facets, index) {
  const editSearch = await createScenario(EDIT_SEARCH_PROMPT);

  switch(editSearch.editSearchPrompt) {
    case 'Search Words':
      const newSearchWord = await createScenario(EDIT_SEARCH_KEY(
        'searchWord', 'Search Word', searchWords));
      await editInteractiveSearch(newSearchWord.searchWord, facets, index);
      break;
    case 'Facet Filters':
      const newFacet = await createScenario(EDIT_SEARCH_KEY(
        'facet', 'Facets', facets));
      await editInteractiveSearch(searchWords, newFacet.facet, index);
      break;
    case 'Search Again':
      await interactiveSearch(index, false, searchWords, facets);
      break;
    case 'Exit':
      break;
  }
}

/**
 * Formats the hits from the algolia search for the table
 * with categories "Name", "Categories" and "Price"
 * @param tableData {Array} Unformatted response from algolia search
 * @returns {Array} Formatted data by categories ['name','categories','price']
 */
function formatTableDataForNameCategoriesPrice(tableData) {
  let formattedData = [];

  tableData.forEach((entry) => {
    const name = entry.name;
    const categories = mergeArrayIntoStringWithNewlines(entry.categories);
    const price = entry.price;

    formattedData.push([name, categories, price]);
  });

  return formattedData;
}
