import Clitable from 'cli-table3';

/**
 * Creates a table string with cli-table3
 * @param headers {Array} sets the headers of the table
 * @param tableData {Array} pushes all elements into the table to be drawn
 * @returns {Promise<CliTable3.Table | CliTable3>}
 */
export async function generateTable(headers, tableData) {
  let table = new Clitable({
    head: headers,
  });

  tableData.forEach((entry) => {
   table.push(entry);
  });

  return table;
}
