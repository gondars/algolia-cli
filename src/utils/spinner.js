import ora from 'ora';
import clear from 'clear';

let spinner;

/**
 * Clears the screen with clear() and starts the ORA spinner with
 * @param text {string} which is displayed while spinning
 */
export function startSpinner(text) {
  clear();
  spinner = ora(`${text} \n`);
  spinner.start();
}

/**
 * Stops the ORA spinner
 */
export function stopSpinner() {
  spinner.stop();
}
