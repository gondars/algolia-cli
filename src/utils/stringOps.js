/**
 * Merges all array entries after the first into a string with spaces (example: ['Hm', 'Hello', 'There']
 * @param array {Array}
 * @returns {string} formatted string (example: 'Hello There')
 * Used in formatting search criteria
 */
export function mergeArrayIntoStringWithSpaces(array) {
  let text = '';
  array.forEach((entry, index) => {
    if (index === 0) {
      return;
    } else if (index < array.length - 1) {
      text += entry + ' ';
    } else {
      text += entry;
    }
  });
  return text;
}

/**
 * Merges entries into a string with newlines (example: ['Hello', 'There'])
 * @param array {Array}
 * @returns {string} formatted string with newlines (example: 'Hello\nThere')
 */
export function mergeArrayIntoStringWithNewlines(array) {
  let text = '';
  array.forEach((entry, index) => {
    if (index < array.length - 1) {
      text += entry + '\n';
    } else {
      text += entry;
    }
  });
  return text;
}

/**
 * Splits facet filters using regex to get their key:value pairs
 * @param string of unformatted facet filters (example: brand:apple,categories"Iphone Accesories")
 * @returns {Array} of formatted key:value pairs (example ['brand:apple', 'categories:"Iphone Accessories")
 */
export function splitFacets(string) {
  if (!string) return [];
	const facetsKeyValuePairs = string.match(/[^"':,]+/gm);
	let facets = [];
	for (let i = 0; i < facetsKeyValuePairs.length; i += 2) {
		facets.push(`${facetsKeyValuePairs[i]}:${facetsKeyValuePairs[i+1]}`);
	}
	return facets;
}
