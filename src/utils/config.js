import conf from 'conf';

const config = new conf();
const confPath = 'algolia-cli';

/**
 * Gets the Api Key from the config
 * @returns {string}
 */
export function getApiKey() {
  return config.get(`${confPath}.apiKey`);
}

/**
 * Sets the Api Key in the config
 * @param {string} value
 */
export function setApiKey(value) {
  config.set(`${confPath}.apiKey`, value)
}

/**
 * Gets the Application Id from the config
 * @returns {string}
 */
export function getAppId() {
  return config.get(`${confPath}.appId`);
}

/**
 * Sets the Application Id in the config
 * @param {string} value
 */
export function setAppId(value) {
  config.set(`${confPath}.appId`, value)
}

/**
 * Gets the Algolia Index Name from the config
 * @returns {string}
 */
export function getIndexName() {
  return config.get(`${confPath}.indexName`);
}

/**
 * Sets the Algolia Index Name in the config
 * @param {string} value
 */
export function setIndexName(value) {
  config.set(`${confPath}.indexName`, value)
}

/**
 * Clears all configurations
 */
export function reset() {
  config.clear();
}

