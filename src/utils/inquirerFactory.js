const inquirer = require('inquirer');

/**
 *
 * @param prompts {Array} of {Object}s with inquirer syntax
 * @returns {Promise<void>} returns answers for the prompt as an {object}
 */
export async function createScenario(prompts) {
  return await inquirer.prompt(prompts);
}
