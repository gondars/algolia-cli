const chalk = require('chalk');

export const APPID_APIKEY_INDEXNAME_PROMPT = [
  {
    type: 'input',
    name: 'appId',
    message: chalk`${chalk.green('Please add your Algolia Application Id')}`
  },
  {
    type: 'input',
    name: 'apiKey',
    message: chalk`${chalk.green('Please add your Algolia API Key')}`
  },
  {
    type: 'input',
    name: 'indexName',
    message: chalk`${chalk.green('Please add your Algolia Index Name')}`
  }];

export const INIT_WELCOME = chalk`` +
	`${chalk.green('|-------------------------------------------------------------------------------|')}\n` +
	`${chalk.green('| W E L C O M E   T O   T H E   A L G O L I A - C L I   C O N F I G U R A T O R |')}\n` +
	`${chalk.green('|-------------------------------------------------------------------------------|')}\n`;

/**
 * @return {string}
 */
export function SUCCESS(appId, apiKey, indexname) {
	return chalk`${chalk.green.bold('APPLICATION CONFIGURED SUCCESSFULLY!')}\n\n` +
		`  Your Algolia Application Id:   ${chalk.green(appId)}\n` +
		`  Your Algolia API Key:          ${chalk.green(apiKey)}\n` +
		`  Your Algolia Index Name:       ${chalk.green(indexname)}\n` +
		`\nYou are ready for searching now! please check ${chalk.green('algolia-cli search --help')} on how to do so!`;
}
