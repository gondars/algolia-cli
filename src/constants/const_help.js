const chalk = require(`chalk`);


export const HELP = chalk`` +
  `${chalk.green('Usage')}: algolia-cli <command> [options]\n` +
  `\n` +
  `${chalk.green('Options')}:\n` +
  `  version, --version                  output the version number\n` +
  `  help, --help                        output usage information\n` +
  `\n` +
  `${chalk.green('Commands')}:\n` +
  `  search           		      starts an interactive search menu, which allows you\n` +
  `                                      to input certain criteria and filters and edit previous search\n` +
  `\n` +
  `  search <criteria> [filters]         searches for certain items with the criteria and options\n` +
	`	  			      ${chalk.bold('—example: search iPhone 7 -f categories:”iPhone accessories”')}\n` +
  `\n` +
  `  init			              starts an interactive configuration menu, which allows you\n` +
  `                                      to set up the API Key, Application Id and the Algolia Index name\n` +
  `\n` +
  `  init [options] 	              adds the configuration needed for the application to run\n` +
	`	  			      ${chalk.bold('—example: init --appid <AppId> --apikey <ApiKey> --indexname <IndexName>\n')}` +
  `\n` +
  `  Run algolia-cli <command> --help for detailed usage of given command.`;
export const HELP_SEARCH = chalk`` +
  `${chalk.green('Commands')} for ${chalk.green.bold('<search>')}:\n` +
  `  search           		    starts an interactive search menu, which allows you\n` +
  `                                    to input certain criteria and filters and edit previous search\n` +
  `\n` +
  `  search <criteria> [filters]       searches for certain items with the criteria and options\n` +
  `				    ${chalk.bold('—example: search iPhone 7 -f categories:”iPhone accessories”')}\n` +
	`${chalk.green('Options')} for ${chalk.green.bold('<search>')}:\n` +
	`  -f           		            filters your search with facets, split by "," and value surrounded with braces\n` +
	`  				    ${chalk.bold('—example: search iphone 7 -f brand:"incipio",categories:"iphone accessories"')}\n`;

export const HELP_INIT = `` +
	`${chalk.green('Commands')} for ${chalk.green.bold('<init>')}:\n` +
	`  init			            starts an interactive configuration menu, which allows you\n` +
  `                                    to set up the API Key, Application Id and the Algolia Index name\n` +
  `\n` +
  `  init [options] 	            adds the configuration needed for the application to run\n` +
  `  				    ${chalk.bold('—example: init --appid <AppId> --apikey <ApiKey> --indexname <IndexName>')}\n`;
