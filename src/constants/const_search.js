const chalk = require('chalk');

export const TABLE_HEADERS = ['Name', 'Categories', 'Price'];

export const SEARCH_PROMPT = [
  {
    type: 'input',
    name: 'searchCriteria',
    message: 'What would you like to search for?'
  },
  {
    type: 'input',
    name: 'facets',
    message: 'Would you like to add any facet filters to your search?'
  }];

export const AFTER_SEARCH_PROMPT = [
  {
    type: 'list',
    name: 'afterSearchChoices',
    message: 'What would you like to do next?',
    choices: ['New Search', 'Edit Previous Search', 'Exit']
  }];

export const EDIT_SEARCH_PROMPT = [
  {
    type: 'list',
    name: 'editSearchPrompt',
    message: 'Which would you like to change?',
    choices: ['Search Words', 'Facet Filters', 'Search Again', 'Exit']
  }];

export function EDIT_SEARCH_KEY(key, keyName, prevValue) {
  return [
    {
      type: 'input',
      name: key,
      message: `Insert new ${keyName}`,
      default: prevValue
    }
  ]
}

export const SEARCH_WELCOME = chalk`` +
	`${chalk.green('|-----------------------------------------------------------------------|')}\n` +
	`${chalk.green('| W E L C O M E   T O   T H E   A L G O L I A - C L I   S E A R C H E R |')}\n` +
	`${chalk.green('|-----------------------------------------------------------------------|')}\n`;
