export const mergeArrayIntoStringWithSpacesData = [
  {
    "input": [
      "Hmm",
      "Hello",
      "There"
    ],
    "output": "Hello There"
  },
  {
    "input": [
      "AAAAAA",
      "adfasdf",
      "fdfsdfsdfa",
      "f_212312daxzcz",
      "_213_"
    ],
    "output": "adfasdf fdfsdfsdfa f_212312daxzcz _213_"
  }
];

export const mergeArrayIntoStringWithNewlinesData = [
	{
		"input": [
			"Hmm",
			"Hello",
			"There"
		],
		"output": "Hmm\nHello\nThere"
	},
	{
		"input": [
			"AAAAAA",
			"adfasdf",
			"fdfsdfsdfa",
			"f_212312daxzcz",
			"_213_"
		],
		"output": "AAAAAA\nadfasdf\nfdfsdfsdfa\nf_212312daxzcz\n_213_"
	}
];

export const splitFacetsData = [
	{
		"input": "brand:apple",
		"output": ["brand:apple"]
	},
	{
		"input": "brand:apple,categories:'iphone accessories'",
		"output": ["brand:apple", "categories:iphone accessories"]
	},
	{
		"input": '',
		"output": []
	},
	{
		"input": "brand:apple cat iph acce sori // ^a cc: val:something:wow:split",
		"output": ["brand:apple cat iph acce sori // ^a cc"," val:something","wow:split"]
	}
];
