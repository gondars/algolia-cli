export const versionCMDData = [ '/usr/local/bin/node',
	'/usr/local/bin/algolia-cli',
	'--version' ];

export const versionWithoutDashesCMDData = [ '/usr/local/bin/node',
	'/usr/local/bin/algolia-cli',
	'version' ];

export const helpCMDData = [ '/usr/local/bin/node',
	'/usr/local/bin/algolia-cli',
	'--help' ];

export const wrongCMDData = [ '/usr/local/bin/node',
	'/usr/local/bin/algolia-cli',
	'--wrongCOMMAND' ];

export const searchCMDData = [ '/usr/local/bin/node',
	'/usr/local/bin/algolia-cli',
	'search' ];

export const initCMDData = [ '/usr/local/bin/node',
	'/usr/local/bin/algolia-cli',
	'init' ];

export const biggerSearchCMDData = [ '/usr/local/bin/node',
	'/usr/local/bin/algolia-cli',
	'search',
	'iphone',
	'7',
	'-f',
	'brand:incipio,categories:iphone accessories' ];

export const biggerSearchCMDDataOutput = {
	"_": ["search", "iphone", 7],
	"f": "brand:incipio,categories:iphone accessories"
};
