import packagejson from '../../package';

const cli = require('../../src/cli');
const cliData = require('./testData/cliData');
const helpConst = require('../../src/constants/const_help');
const init = require( '../../src/commands/init');
const search = require('../../src/commands/search');

describe('CLI tests', () => {
	let consoleLogSpy;

	beforeEach(() => {
		consoleLogSpy = jest.spyOn(console, 'log');
	});

	test('run CLI with --version', () => {
		cli.cli(cliData.versionCMDData);
		expect(consoleLogSpy.mock.calls)
			.toEqual([[`Algolia-CLI current version: ${packagejson.version}`]])
	});

	test('run CLI with version', () => {
		cli.cli(cliData.versionWithoutDashesCMDData);
		expect(consoleLogSpy.mock.calls)
			.toEqual([[`Algolia-CLI current version: ${packagejson.version}`]])
	});

	test('run CLI with --help', () => {
		cli.cli(cliData.helpCMDData);
		expect(consoleLogSpy.mock.calls)
			.toEqual([[helpConst.HELP]]);
	});

	test('run CLI with wrong command, should output help',() => {
		cli.cli(cliData.wrongCMDData);
		expect(consoleLogSpy.mock.calls)
			.toEqual([[helpConst.HELP]]);
	});

	test('run CLI with search command', () => {
		search.search = jest.fn().mockResolvedValue();
		cli.cli(cliData.searchCMDData);
		expect(search.search).toHaveBeenCalledWith({'_': ['search']});

	});

	test('run CLI with init command', () => {
		init.init = jest.fn().mockResolvedValue();
		cli.cli(cliData.initCMDData);
		expect(init.init).toHaveBeenCalledWith({'_': ['init']});
	});

	test('properly passes args to search', () => {
		search.search = jest.fn().mockResolvedValue();
		cli.cli(cliData.biggerSearchCMDData);
		expect(search.search).toHaveBeenCalledWith(cliData.biggerSearchCMDDataOutput);
	});
});
