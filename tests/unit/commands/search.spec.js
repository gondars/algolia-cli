import {getApiKey, getAppId, getIndexName} from '../../../src/utils/config';

const search = require('../../../src/commands/search');
const searchTestData = require('../testData/searchTestData');
const constants = require('../../../src/constants/const_search');
const helpConstants = require('../../../src/constants/const_help');

import algoliasearch from 'algoliasearch';

describe('tests for search.js', () => {

	let consoleLogSpy;

	const client = algoliasearch('','');
	const index = client.initIndex('');

	beforeEach(() => {
		consoleLogSpy = jest.spyOn(console, 'log');
	});

	test('search --help', async () => {
		await search.search({_:['search'], help: true});
		expect(consoleLogSpy.mock.calls)
			.toEqual([[helpConstants.HELP_SEARCH]]);
	});

	///TODO see why mocking doesn't work on searchFor
	test.skip('search samsung', async () => {
		let searchSpy = jest.spyOn(search, 'searchFor');
		await search.search({_: ['search', 'samsung']});
		expect(searchSpy).toBeCalled()
	});
});
