const init = require('../../../src/commands/init');
const initTestData = require('../testData/initTestData');
const constants = require('../../../src/constants/const_init');
const help_constants = require('../../../src/constants/const_help');
import inquirer from 'inquirer';

describe('tests for init.js', () => {

	let testData = { _: [ 'init' ],
		appid: 'OSO639TGSW',
		apikey: '71a3d2ddeb384202b997208d35476a5b',
		indexname: 'test_clippings' };

	let moredata = { appId: 'OSO639TGSW',
		apiKey: '71a3d2ddeb384202b997208d35476a5b',
		indexName: 'test_clippings' };

	let consoleLogSpy;

	beforeEach(() => {
		consoleLogSpy = jest.spyOn(console, 'log');
	});

	test('init with args', async () => {
		await init.init(testData);
		expect(consoleLogSpy.mock.calls)
			.toEqual([[constants.SUCCESS(testData.appid, testData.apikey, testData.indexname)]]);
	});

	test('init with interactive', async () => {
		inquirer.prompt = jest.fn().mockResolvedValue(moredata);
		await init.init({_:['init']});
		expect(consoleLogSpy.mock.calls)
			.toEqual([[constants.INIT_WELCOME],[constants.SUCCESS(testData.appid, testData.apikey, testData.indexname)]]);
	});

	test('init with --help', async () => {
		await init.init({_:['init'], help: true});
		expect(consoleLogSpy.mock.calls)
			.toEqual([[help_constants.HELP_INIT]]);
	});
});


