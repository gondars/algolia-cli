const stringOps = require('../../../src/utils/stringOps');
const testData = require('../testData/stringOpsData');

describe('stringOps tests', () => {
	test('mergeArrayIntoStringWithSpaces', () => {
		testData.mergeArrayIntoStringWithSpacesData.forEach((entry) => {
			expect(stringOps.mergeArrayIntoStringWithSpaces(entry.input)).toEqual(entry.output);
		})
	});

	test('mergeArrayIntoStringWithNewlines', () => {
		testData.mergeArrayIntoStringWithNewlinesData.forEach((entry) => {
			expect(stringOps.mergeArrayIntoStringWithNewlines(entry.input)).toEqual(entry.output);
		})
	});

	test('splitFacets', () => {
		testData.splitFacetsData.forEach((entry) => {
			expect(stringOps.splitFacets(entry.input)).toEqual(entry.output);
		});
	})
});
