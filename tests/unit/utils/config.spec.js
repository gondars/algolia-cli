const config = require('../../../src/utils/config');

describe("Config Tests", () => {
  test('setApiKey', () => {
    const apiKey = '71a3d2ddeb384202b997208d35476a5b';
    config.setApiKey(apiKey);
		expect(config.getApiKey()).toEqual(apiKey);
	});
  test('setAppId', () => {
    const appId = 'OSO639TGSW';
		config.setAppId(appId);
    expect(config.getAppId()).toEqual(appId);
	});
  test('setIndexName', () => {
    const indexName = 'test_clippings';
    config.setIndexName(indexName);
    expect(config.getIndexName()).toEqual(indexName);
  });
  test('reset', () => {
    config.reset();
    expect(config.getAppId()).toEqual(undefined);
    expect(config.getApiKey()).toEqual(undefined);
    expect(config.getIndexName()).toEqual(undefined);
  })
});
