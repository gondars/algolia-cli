const inquirerFactory = require('../../../src/utils/inquirerFactory');
import inquirer from 'inquirer';

describe('Inquirer Factory Tests', () => {

	test('createScenario', async () => {
		const data = [
			{
				type: 'input',
				name: 'searchCriteria',
				message: 'What would you like to search for?'
			},
			{
				type: 'input',
				name: 'facets',
				message: 'Facets?'
			}];
		inquirer.prompt = () => Promise.resolve({answer1: 'answer1', answer2: 'answer2'});
		const answers = await inquirerFactory.createScenario(data);
		expect(answers.answer1).toEqual('answer1');
		expect(answers.answer2).toEqual('answer2');
	});

});
