# Algolia-CLI

Algolia-CLI is a NodeJS CLI for fetching data from Algolia.

## Installation

Use the NodeJS Package Manager [NodeJS](https://nodejs.org/en/) to install Algolia-CLI.

```bash
npm install algolia-cli
```

## Configuration
You need to get your Index Name, Application Id and API Key from Algolia, then you need to add it to the configs. You can do it in two ways:

```
algolia-cli init --appid {APPID} --apikey {APIKEY} --indexname {INDEXNAME}
```
or through the init menu, which is triggered by 
```
algolia-cli init
```
It will ask you for the API Key, Application Id and Index name afterwards.

DEMO:

![Alt Text](https://s7.gifyu.com/images/algoliacliinit.gif)

## Usage
You can use it to search inside your Algolia Index for any data manually, or through a interactive menu.
```
algolia-cli search iPhone 7
```
This will show you all results matching 'iPhone 7'. If you want to add facet filters to it, you can add them by entering -f and the facets!
```
algolia-cli search iPhone 7 -f categories:"iPhone accessories"
```
You can add more filters by separating them with ','
```
algolia-cli search iPhone 7 -f categories:"iPhone accessories",brand:"incipio"
```
You can access the interactive search by not adding any parameters at all!
```
algolia-cli search
```
It will ask you about your search and filters, then it will display
your results, and then you can edit your previous search

DEMO:

![Alt Text](https://s7.gifyu.com/images/algoliainitsearch.gif)

You can also access the HELP menu with --help
```
algolia-cli --help
```
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
MIT License

Copyright (c) 2020 Valentin Panchev

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

##About the Author
Valentin is a full-stack developer, who has never done a CLI before
